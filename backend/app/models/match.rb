class Match < ApplicationRecord
  require "net/http"

  def self.fetch_data
    url = "http://api.football-data.org/v1/fixtures"
    response = Net::HTTP.get_response(URI.parse(url))
    parsed_response = JSON.parse(response.body)
    parsed_response["fixtures"].each do |value|
      Match.find_or_create_by!(date: value["date"],
                               home_team_name: value["homeTeamName"],
                               away_team_name: value["awayTeamName"],
                               home_team_result: value["result"]["goalsHomeTeam"],
                               away_team_result: value["result"]["goalsHomeTeam"])
    end
 end
end
