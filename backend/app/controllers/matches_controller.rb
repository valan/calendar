class MatchesController < ApplicationController
  def index
    Match.fetch_data
    data = params[:date].present? ? Match.where(date: Date.parse(params[:date])) : nil
    render json: data
  end
end
