class CreateMatches < ActiveRecord::Migration[5.1]
  def change
    create_table :matches do |t|
      t.date :date
      t.string :home_team_name
      t.string :away_team_name
      t.integer :home_team_result
      t.integer :away_team_result
    end
  end
end
