import api from 'api';

export const FETCH_MATCHES = 'FETCH_MATCHES';
export const SHOW_MATCHES = 'SHOW_MATCHES';

export function showMatches(date) {
  return function (dispatch) {
    api.fetchData(date).then(data => dispatch(returnMatches(data)));
  };
}

export function returnMatches(data) {
  return {
    type: SHOW_MATCHES,
    data
  };
}
