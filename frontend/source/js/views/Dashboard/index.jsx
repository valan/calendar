import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { showMatches } from 'actions/app';
import InfiniteCalendar from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css';

@connect(state => ({
  matches: state.app.get('matches'),
}))

export default class Dashboard extends Component {
  static propTypes = {
    matches: PropTypes.array,
    // from react-redux connect
    dispatch: PropTypes.func,
  }
  constructor() {
    super();

    this.handleDateButtonClick = this.handleDateButtonClick.bind(this);
  }
  handleDateButtonClick(date) {
    const { dispatch } = this.props;

    dispatch(showMatches(date));
  }
  render() {
    let today = new Date();
    let lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
    return (
      <div className='Dashboard'>
        <InfiniteCalendar
          selected={today}
          disabledDays={[0,6]}
          minDate={lastWeek}
          onSelect={(date) => this.handleDateButtonClick(date)}
        />
      </div>
    );
  }
}
