import { Map } from 'immutable';

import { SHOW_MATCHES } from 'actions/app';

const initialState = Map({
  matches: [],
});

const actionsMap = {
  [SHOW_MATCHES]: (state, action) => {
    return state.merge(Map({
      matches: action.data,
    }));
  },
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
