import promisePolyfill from 'es6-promise';
import 'isomorphic-fetch';

promisePolyfill.polyfill();

function fetchData(date) {
  return fetch('http://localhost:4000/matches', { date: date })
    .then(response => response.json());
}

export default {
  fetchData,
};
